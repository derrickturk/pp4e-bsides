use rusty_vre::*;

fn approx_eq(x: f64, y: f64, tol: f64) -> bool {
    (x - y).abs() <= tol
}

#[test]
fn test_tpc() {
    assert!(approx_eq(tpc(0.699), 389.1, 0.1));
}

#[test]
fn test_ppc() {
    assert!(approx_eq(ppc(0.699), 669.2, 0.1));
}

#[test]
fn test_z() {
    assert!(approx_eq(z(640.0,3000.0,0.699), 0.824 ,0.1));
}

#[test]
fn test_gas_oil_ratio() {
    assert!(approx_eq(gas_oil_ratio(0.851, 250.0+459.67, 47.1, 2377.0+14.7), 737.0, 1.0));
    assert!(approx_eq(gas_oil_ratio(0.911, 260.0+459.67, 48.6, 2051.0+14.7), 686.0, 1.0));
}
