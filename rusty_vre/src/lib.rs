///Volumetric Reserves Estimation: the land of f64's.

//This lets us use #[derive(Deserialize)] tags.
use serde::{Deserialize, Serialize};

///phase classification. Can be 0 or 1;
#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "lowercase")]
pub enum Phase {
    Oil = 0,
    Gas = 1,
}

///Known properties of an oil or gas asset.
#[derive(Debug, Deserialize, Serialize)]
pub struct Asset{
    #[serde(rename = "Asset_Name")]
    pub name: String,
    #[serde(rename = "Reservoir_Area")]
    pub area: f64,
    #[serde(rename = "Reservoir_Net_Height")]
    pub height: f64,
    #[serde(rename = "Fluid_Phase")]
    pub phase: Phase,
    #[serde(rename = "Estimated_Recovery_Factor")]
    pub recovery_factor: f64,
    #[serde(rename = "Average_Porosity")]
    pub porosity: f64,
    #[serde(rename = "Average_Sw")]
    pub sw: f64,
    #[serde(rename = "Average_Reservoir_Temp")]
    pub temp: f64, //assumes in F.
    #[serde(rename = "Average_Reservoir_Pres")]
    pub pressure: f64,
    #[serde(rename = "Oil_API")]
    #[serde(deserialize_with = "csv::invalid_option")]
    pub api: Option<f64>,
    #[serde(rename = "Gas_Sg")]
    pub gas_sg: f64,
}

///Takes an Asset, matches oil or gas. Returns original_in_place amount.
pub fn asset_original_in_place(asset: &Asset) -> f64 {
    match asset.phase {
        Phase::Oil => {
            assert!(asset.api != None);
            let bo:f64 = oil_fvf_bo(asset.gas_sg, asset.temp + 459.67, asset.api.unwrap(), asset.pressure);
            return original_oil_in_place(asset.area, asset.height, asset.porosity, asset.sw, bo);
        }

        Phase::Gas => {
            let bg:f64 = gas_fvf_bg(asset.temp + 459.67, asset.pressure, asset.gas_sg);
            return original_gas_in_place(asset.area, asset.height, asset.porosity, asset.sw, bg);
        }
    }
}

///EUR - Estimated Ultimate Recovery. (bbl or Mscf^3)
///Inputs: original (oil or gas) in place, recovery factor.
pub fn eur(original_in_place:f64, rf:f64) -> f64 {
    original_in_place*rf
}

///OOIP (original oil in place) in barrels.
///Inputs are: area in acres (area), net pay height in feet (height),
///porosity (porosity), water saturation (sw), and formation volume factor.
pub fn original_oil_in_place(area: f64, height: f64, porosity: f64, sw: f64, bo:f64) -> f64 {
    7758.0 * area * height * porosity * (1.0 - sw) / bo
}

///OGIP (original gas in place) in thousand standard ft^3
///Inputs are: area in acres (area), net pay height in feet (height),
///porosity (porosity), water saturation (sw), and formation volume factor.
pub fn original_gas_in_place(area: f64, height: f64, porosity: f64, sw: f64, bg:f64) -> f64 {
    43.560 * area * height * porosity * (1.0 - sw) / bg
}

///Gas only Formation Volume Factor (ft^3/scf)
///This is using the ideal gas law.
///Inputs are: temperature in R (t), pressure in psi (p), and z, the compressibility factor.
pub fn gas_fvf_bg(t: f64, p: f64, yg:f64) -> f64 {
    0.02827 * (z(t, p, yg) * t) / p
}

///Saturated Oil Formation Volume Factor (bbl/stb) where p=pb. Returns bo - the oil formation volume factor.
///Inputs are: yg, Temp in R (t), API (api), gas_oil_ratio in scf/bbl (gor)
///NOTE: for testing, the gor formula was replaced with a gor number. This has been commented out in the test list.
pub fn oil_fvf_bo(yg: f64, t:f64, api: f64, p:f64) -> f64 {
    let y_o = 141.5 / (131.5 + api);
    let b_ob = gas_oil_ratio(yg, t, api, p ) * (yg / y_o).powf(0.526) + 0.968 * (t - 459.67);

    let random_garbage: f64 = -6.58511 + 2.91329 * b_ob.log10() - 0.27683 * (b_ob.log10()).powf(2.0);
    10f64.powf(random_garbage) + 1.0
}

///gas-oil ratio. Inputs are specific gravity of the solution or gas: yg, Temp in R (t), API gravity (api), and pressure (p).
///NOTE: Temp is in Rankine. This differs from the SW slides.
pub fn gas_oil_ratio(yg: f64, t: f64, api: f64, p: f64) -> f64 {
    let random_garbage: f64 = 2.8869 - (14.1811 - 3.3093 * p.log10()).powf(0.5);
    let a = 10_f64.powf(random_garbage);

    yg * (a * (api.powf(0.989) / (t - 459.67).powf(0.172))).powf(1.2255)
}

///estimated compressibility factor for a natural gas (z).
///Inputs are: temp in R (t), pressure in psi (p), and specific gravity of gas (yg).
pub fn z(t: f64, p: f64, yg:f64) -> f64 {
    let t_pr = t/tpc(yg);
    let p_pr = p/ppc(yg);

    1.0-((3.53 * p_pr) / (10.0_f64.powf(0.9813 * t_pr))) + ((0.274 * (p_pr * p_pr)) / (10.0_f64.powf(0.8157 * t_pr)))
}

///pseudo-critical temperature in R (t_pc)
pub fn tpc(yg: f64) -> f64{
    168.0 + 325.0 * yg - 12.5 * (yg * yg)
}

///pseudo-critical pressure in psi (p_pc)
pub fn ppc(yg: f64) -> f64{
    677.0 + 15.0 * yg - 37.5 * (yg * yg)
}
