use rusty_vre::*;
use std::{env, ffi::OsString, error::Error};

fn get_args(n:usize) -> Result<OsString, Box<dyn Error>> {
    match env::args_os().nth(n) {
        Some(file_path) => Ok(file_path),
        None => {
            let msg = format!(
                "expected csv argument at {}, but got none. Please supply a source and destination csv file.", n);
            Err(msg)?
        },
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    //read csv, map to struct Asset.

    //Find the csv read & write file.
    let file_path = get_args(1)?;
    let write_path = get_args(2)?;
    //Create the write file:
    let mut wtr = csv::Writer::from_path(write_path)?;
    wtr.write_record(&["Asset_Name", "Fluid_Phase", "Original_in_Place", "EUR"])?;

    //Read the CSV file & write to new CSV file.
    let mut rdr = csv::Reader::from_path(file_path)?;
    for result in rdr.deserialize(){
        let record: Asset = result?;

        let original_in_place:f64 = asset_original_in_place(&record);
        let eur: f64 = eur(original_in_place, record.recovery_factor);
        wtr.serialize((&record.name, &record.phase, &original_in_place, &eur))?;
    }

    wtr.flush()?;

    Ok(())
}
