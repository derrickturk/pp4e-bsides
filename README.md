# PP4E B-Sides

Alternate implementations of case study or example code from PP4E.
  - monty-rs: an implementation of the Chapter 4/5 case study in Rust, approximately 17x faster than Python and 10x faster than Python + numpy
  - monty-jl: same, but in Julia, with performance similar to Rust after the 3rd run (due to JIT overhead and GC pauses)
  - monty-hs: same, but in Haskell, approximately 2.8x faster than python + numpy - and purely functional!
  - monty++: a zero-runtime implementation of the Chapter 4/5 case study in C++. That's not an exaggeration - the entire model executes during compilation. Compiling the program generates trials about 20-30x slower than interpreting the pure Python version from Chapter 3. Requires a (mostly) C++20-compliant compiler with `constexpr std::pow`, so pretty much just `g++`.
  - rusty-vre: an implementation of the Chapter 3 case study in Rust. Very fast. Does take an input CSV file (use ch3_data.csv)
  - examples/reverse.cpp: a C++ implementation of the "reverse a binary tree" problem
  - timings.txt: very unscientific "on my machine" comparison of Chapter 4/5 case study implementation performance
  

### (c) 2020 dwt | terminus data science, LLC
