use super::model::{Project};

use lazy_static::lazy_static;

mod bat_country;
pub use bat_country::*;
mod needs_more_bats;
pub use needs_more_bats::*;
mod bat_classic;
pub use bat_classic::*;
mod beyond_the_pale;
pub use beyond_the_pale::*;
mod high_water_mark;
pub use high_water_mark::*;

lazy_static! {
    pub static ref ROOT_PROJECTS: [&'static Project<'static>; 2] = [
        &BAT_COUNTRY,
        &BEYOND_THE_PALE,
    ];
}
