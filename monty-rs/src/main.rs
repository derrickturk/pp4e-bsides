#![feature(const_mut_refs)]

mod model;
mod monte;
mod projects;

use model::{
    PriceDeck,
    net_cashflow,
    npv,
};
use monte::trials;
use projects::{ROOT_PROJECTS};

use lazy_static::lazy_static;

use ndarray::{Array, Array1};

use rand::prelude::*;

const TRIALS: usize = 10000;
const DISCOUNT: f64 = 0.10;

lazy_static! {
    static ref FLAT: PriceDeck = PriceDeck {
        oil_price: Array::from_elem((36,), 40.0),
        gas_price: Array::from_elem((36,), 2.50),
    };
}

#[derive(Copy, Clone, Debug)]
struct Stats {
    mean: f64,
    p90: f64,
    p50: f64,
    p10: f64,
}

fn extract_stats(npvs: &Array1<f64>) -> Stats {
    let n = npvs.len();
    Stats {
        mean: npvs.mean().unwrap(),
        p90: npvs[(n as f64 * 0.1) as usize],
        p50: npvs[(n as f64 * 0.5) as usize],
        p10: npvs[(n as f64 * 0.9) as usize],
    }
}

fn main() {
    let mut rng = thread_rng();

    let now = std::time::Instant::now();
    let ts = trials(&mut rng, &ROOT_PROJECTS[..], |rs| {
        let total_cf = rs.fold(Array1::<f64>::zeros((36,)),
          |cf, (p, o)| cf + net_cashflow(p, o, &FLAT));
        npv(&total_cf, DISCOUNT)
    });

    let mut npvs = ts.take(TRIALS).collect::<Vec<_>>();
    let elapsed = now.elapsed();
    dbg!(elapsed);

    npvs.sort_by(|a, b| a.partial_cmp(b).unwrap());
    let npvs: Array1<_> = npvs.into();
    let stats = extract_stats(&npvs);
    dbg!(stats);
}
