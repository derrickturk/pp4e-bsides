use ndarray::Array1;

#[derive(Copy, Clone, Debug)]
pub struct Project<'a> {
    pub name: &'a str,
    pub oil_shrink: f64,
    pub gas_shrink: f64,
    pub wi: f64,
    pub nri: f64,
    pub tax_rate: f64,
    pub outcomes: &'a [(f64, &'a Outcome<'a>)],
}

#[derive(Clone, Debug)]
pub struct Outcome<'a> {
    pub oil: Array1<f64>,
    pub gas: Array1<f64>,
    pub capex: Array1<f64>,
    pub opex: Array1<f64>,
    pub leads_to: &'a [&'a Project<'a>]
}

pub struct PriceDeck {
    pub oil_price: Array1<f64>,
    pub gas_price: Array1<f64>,
}

pub fn net_cashflow(p: &Project, o: &Outcome, deck: &PriceDeck) -> Array1<f64> {
    let gross_revenue =
      (1.0 - p.oil_shrink) * &o.oil * &deck.oil_price * p.nri +
      (1.0 - p.gas_shrink) * &o.gas * &deck.gas_price * p.nri;
    let net_opex = &o.opex * p.wi * 1000.0;
    let net_capex = &o.capex * p.wi * 1000.0;
    let tax = p.tax_rate * &gross_revenue;
    gross_revenue - net_opex - net_capex - tax
}

pub fn npv(cf: &Array1<f64>, discount: f64) -> f64 {
    let mut time = Array1::range(0.0, cf.len() as f64, 1.0) / 12.0;
    time.mapv_inplace(|t| 1.0 / (1.0 + discount).powf(t));
    (time * cf).sum()
}
