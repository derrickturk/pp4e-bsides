use super::model::{Project, Outcome};

use std::mem;

use rand::prelude::*;

#[inline]
pub fn realize_outcome<'a, R: Rng>(r: &mut R, p: &'a Project<'a>
  ) -> &'a Outcome<'a> {
    p.outcomes.choose_weighted(r, |(prob, _)| *prob).unwrap().1
}

#[inline]
pub fn realizations<'a, R: Rng>(r: &'a mut R, root_projects: &'a [&'a Project<'a>]
  ) -> Realizations<'a, R> {
    Realizations::new(r, root_projects)
}

#[inline]
pub fn trials<'a, R, F, T>(r: &'a mut R, root_projects: &'a [&'a Project<'a>],
  f: F) -> Trials<'a, R, F>
  where R: Rng,
        F: for <'b> Fn(Realizations<'b, R>) -> T {
    Trials {
        rng: r,
        root_projects: root_projects,
        func: f,
    }
}

pub struct Realizations<'a, R> {
    rng: &'a mut R,
    frame: Frame<'a>,
    yolt: Option<&'a Outcome<'a>>,
    stack: Vec<Frame<'a>>,
}

struct Frame<'a> {
    root_projects: &'a [&'a Project<'a>],
    ix: usize,
}

impl<'a, R> Realizations<'a, R> {
    fn new(rng: &'a mut R, root_projects: &'a [&'a Project<'a>]) -> Self {
        Self {
            rng,
            frame: Frame {
                root_projects,
                ix: 0,
            },
            yolt: None,
            stack: Vec::new(),
        }
    }
}

impl<'a, R> Iterator for Realizations<'a, R> where R: Rng {
    type Item = (&'a Project<'a>, &'a Outcome<'a>);

    fn next(&mut self) -> Option<Self::Item> {
        if self.frame.ix >= self.frame.root_projects.len() {
            if self.stack.is_empty() {
                None
            } else {
                self.frame = self.stack.pop().unwrap();
                self.next()
            }
        } else {
            if let Some(o) = self.yolt.take() {
                self.frame.ix += 1;
                let mut new_frame = Frame {
                    root_projects: o.leads_to,
                    ix: 0,
                };
                mem::swap(&mut new_frame, &mut self.frame);
                self.stack.push(new_frame);
                self.next()
            } else {
                let p = &self.frame.root_projects[self.frame.ix];
                let o = realize_outcome(self.rng, p);
                self.yolt = Some(o);
                Some((p, o))
            }
        }
    }
}

pub struct Trials<'a, R, F> {
    rng: &'a mut R,
    root_projects: &'a [&'a Project<'a>],
    func: F,
}

impl<'a, R, F, T> Iterator for Trials<'a, R, F>
  where R: Rng,
        F: for <'b> Fn(Realizations<'b, R>) -> T {
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        Some((self.func)(realizations(self.rng, self.root_projects)))
    }
}
