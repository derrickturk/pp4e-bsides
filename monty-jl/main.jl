using Random
using Profile

include("model.jl")
include("projects.jl")
include("monte.jl")

const FLAT_DECK = PriceDeck(fill(40.0, (36,)), fill(2.50, (36,)))
const TRIALS = 0x00002710 # wtf
const DISCOUNT = 0.10

function main()
    rng = MersenneTwister(0)
    npvs = collect(monte_carlo_trials(rng, TRIALS, root_projects) do rs
        npv(sum(net_cashflow(p, o, FLAT_DECK) for (p, o) in rs), DISCOUNT)
    end)
end

for _ in 1:10
    @time main() # trigger JIT
end
