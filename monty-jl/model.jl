struct _Outcome{P}
    probability::Float64
    oil::Vector{Float64}
    gas::Vector{Float64}
    capex::Vector{Float64}
    opex::Vector{Float64}
    leads_to::Vector{P}
end

struct Project
    name::String
    oil_shrink::Float64
    gas_shrink::Float64
    wi::Float64
    nri::Float64
    tax_rate::Float64
    outcomes::Vector{_Outcome{Project}}
end

Outcome = _Outcome{Project}

struct PriceDeck
    oil_price::Vector{Float64}
    gas_price::Vector{Float64}
end

function net_cashflow(p::Project, o::Outcome, deck::PriceDeck)::Vector{Float64}
    gross_revenue = @. (1.0 - p.oil_shrink) * o.oil * deck.oil_price * p.nri +
      (1.0 - p.gas_shrink) * o.gas * deck.gas_price * p.nri
    net_opex = @. o.opex * p.wi * 1000.0
    net_capex = @. o.capex * p.wi * 1000.0
    tax = @. p.tax_rate * gross_revenue
    @. gross_revenue - net_opex - net_capex - tax
end

function npv(cf::Vector{Float64}, discount::Float64)::Float64
    time = range(0.0, step=1.0 / 12.0, length=length(cf))
    scalar = @. (1.0 + discount) ^ time
    sum(cf ./ scalar)
end
