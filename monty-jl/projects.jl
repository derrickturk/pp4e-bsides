include("projects/bat_classic.jl")
include("projects/needs_more_bats.jl")
include("projects/bat_country.jl")
include("projects/high_water_mark.jl")
include("projects/beyond_the_pale.jl")

root_projects = [bat_country, beyond_the_pale]
all_projects = [bat_country, beyond_the_pale, bat_classic,
                needs_more_bats, high_water_mark]
