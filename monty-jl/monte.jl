using Random

function realize_outcome(rng::AbstractRNG, p::Project)::Outcome
    q = rand(rng, Float64)
    so_far = 0.0
    for o in p.outcomes
        so_far += o.probability
        if q < so_far
            return o
        end
    end
    error("invalid probability distribution")
end

function realizations(rng::AbstractRNG, root_projects)
    Iterators.flatten(realizations_for(rng, p) for p in root_projects)
end

function realizations_for(rng::AbstractRNG, p::Project)
    o = realize_outcome(rng, p)
    Iterators.flatten(([(p, o)], realizations(rng, o.leads_to)))
end

function monte_carlo_trials(f, rng::AbstractRNG, n::UInt32, root_projects)
    (f(realizations(rng, root_projects)) for _ in 1:n)
end
