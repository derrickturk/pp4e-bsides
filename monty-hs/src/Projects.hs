module Projects (
    batCountry
  , needsMoreBats
  , batClassic
  , beyondThePale
  , highWaterMark
  , allProjects
  , rootProjects
) where

import Model
import Projects.BatCountry
import Projects.NeedsMoreBats
import Projects.BatClassic
import Projects.BeyondThePale
import Projects.HighWaterMark

rootProjects :: [Project]
rootProjects = [ batCountry
               , beyondThePale
               ]

allProjects :: [Project]
allProjects = [ batCountry
              , needsMoreBats
              , batClassic
              , beyondThePale
              , highWaterMark
              ]
