module Monte (
    realizeOutcome
  , realizations
) where

import Control.Monad.Random.Class

import Model

{-# INLINE toRationalWeighted #-}
toRationalWeighted :: (Double, Outcome) -> (Outcome, Rational)
toRationalWeighted (p, o) = (o, toRational p)

realizeOutcome :: MonadRandom m => Project -> m Outcome
realizeOutcome = fromList . fmap toRationalWeighted . outcomes

realizations :: MonadRandom m => [Project] -> m [(Project, Outcome)]
realizations [] = pure []
realizations (p:ps) = do
  o <- realizeOutcome p
  rec <- realizations $ leadsTo o
  rest <- realizations ps
  pure $ (p, o):rec ++ rest
