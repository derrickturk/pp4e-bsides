{-# LANGUAGE StrictData #-}

module Model (
    Project(..)
  , Outcome(..)
  , PriceDeck(..)
  , netCashflow
  , npv
) where

import qualified Data.Text as T
import qualified Data.Vector.Unboxed as V

data Project
  = Project { name :: T.Text
            , oilShrink :: Double
            , gasShrink :: Double
            , wi :: Double
            , nri :: Double
            , taxRate :: Double
            , outcomes :: [(Double, Outcome)]
            } deriving (Eq, Show)

data Outcome
  = Outcome { oil :: V.Vector Double
            , gas :: V.Vector Double
            , capex :: V.Vector Double
            , opex :: V.Vector Double
            , leadsTo :: [Project]
            } deriving (Eq, Show)

data PriceDeck
  = PriceDeck { oilPrice :: V.Vector Double
              , gasPrice :: V.Vector Double
              } deriving (Eq, Show)

netCashflow :: PriceDeck -> Project -> Outcome -> V.Vector Double
netCashflow d p o =
  let oilScalar = (1.0 - oilShrink p) * nri p
      gasScalar = (1.0 - gasShrink p) * nri p
      oilRev = V.map (* oilScalar) $ V.zipWith (*) (oilPrice d) (oil o)
      gasRev = V.map (* gasScalar) $ V.zipWith (*) (oilPrice d) (oil o)
      netOpex = V.map (* (1000.0 * wi p)) $ opex o
      netCapex = V.map (* (1000.0 * wi p)) $ capex o
      rev = V.zipWith (+) oilRev gasRev
      tax = V.map (* (taxRate p)) rev
   in V.zipWith4 (\r o c t -> r - o - c - t) rev netOpex netCapex tax

npv :: Double -> V.Vector Double -> Double
npv d cf =
  let monthlyScalar i = (1.0 + d) ** (fromIntegral i / 12.0)
      scalars = V.generate (V.length cf) monthlyScalar
   in V.sum $ V.zipWith (/) cf scalars
