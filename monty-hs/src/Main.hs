module Main where

import Control.Monad (replicateM)
import qualified Data.Vector.Unboxed as V

import Gauge.Main

import Model
import Monte
import Projects

flat :: PriceDeck
flat = PriceDeck { oilPrice = V.replicate 36 40.0
                 , gasPrice = V.replicate 36 2.50
                 }

discount :: Double
discount = 0.10

newtype SummedCashflow = SummedCashflow { getSummedCashflow :: V.Vector Double }
  deriving (Eq, Show)

instance Semigroup SummedCashflow where
  (SummedCashflow lhs) <> (SummedCashflow rhs) =
    SummedCashflow $ V.zipWith (+) lhs rhs

instance Monoid SummedCashflow where
  mempty = SummedCashflow $ V.replicate 36 0.0

trialTotal :: PriceDeck -> [(Project, Outcome)] -> V.Vector Double
trialTotal d =
  getSummedCashflow . foldMap (SummedCashflow . uncurry (netCashflow d))

runSim :: IO (V.Vector Double)
runSim = do
  trials <- replicateM 10000 $ realizations rootProjects
  pure $ V.fromList $ (npv discount . trialTotal flat) <$> trials

{-
main :: IO ()
main = do
  trials <- replicateM 10000 $ realizations rootProjects
  let npvs = V.fromList $ (npv discount . trialTotal flat) <$> trials
  V.mapM_ print npvs
-}

main :: IO ()
main = defaultMain [ bench "runSim" $ nfIO runSim ]
