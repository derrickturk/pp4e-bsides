#include <memory>
#include <utility>
#include <iostream>

class node {
  public:
    node(int root_val)
        : val_(root_val), left_(), right_() { }

    void insert(int val)
    {
        if (val <= val_) {
            if (left_)
                left_->insert(val);
            else
                left_ = std::make_unique<node>(val);
        } else {
            if (right_)
                right_->insert(val);
            else
                right_ = std::make_unique<node>(val);
        }
    }

    void reverse()
    {
        if (left_)
            left_->reverse();
        if (right_)
            right_->reverse();
        std::swap(left_, right_);
    }

    template<class F>
    void inorder(F&& fn)
    {
        if (left_)
            left_->inorder(std::forward<F>(fn));
        fn(val_);
        if (right_)
            right_->inorder(std::forward<F>(fn));
    }

  private:
    int val_;
    std::unique_ptr<node> left_;
    std::unique_ptr<node> right_;
};

int main()
{
    auto tree = node(5);
    tree.insert(7);
    tree.insert(3);
    tree.insert(2);
    tree.inorder([](int val) { std::cout << val << '\n'; });
    tree.reverse();
    tree.inorder([](int val) { std::cout << val << '\n'; });
}
