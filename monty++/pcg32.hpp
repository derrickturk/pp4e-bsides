#ifndef MONTY_PCG32_HPP

#include <cstdint>
#include <limits>

namespace monty {

class pcg32 {
  public:
    constexpr pcg32(std::uint64_t seed = 0xcafef00dd15ea5e5ul) noexcept
        : state_(bump_(seed + increment_)) { }

    constexpr void advance(std::uint64_t delta) noexcept
    {
        state_ = advance_(state_, delta);
    }

    constexpr std::uint32_t uniform_uint32_t() noexcept
    {
        return output_(generate());
    }

    constexpr double uniform_double(double lower, double upper)
    {
        constexpr auto max = std::numeric_limits<std::uint32_t>::max();
        return lower +
          static_cast<double>(uniform_uint32_t()) / max * (upper - lower);
    }

  private:
    std::uint64_t state_;

    constexpr std::uint64_t generate() noexcept
    {
        auto old_state = state_;
        state_ = bump_(state_);
        return old_state;
    }

    constexpr static std::uint64_t bump_(std::uint64_t state) noexcept
    {
        return state * multiplier_ + increment_;
    }

    constexpr static
    std::uint64_t advance_(std::uint64_t state, std::uint64_t delta) noexcept
    {
        std::uint64_t cur_multiplier = multiplier_, cur_increment = increment_;
        std::uint64_t acc_multiplier = 1, acc_increment = 0;
        while (delta > 0ul) {
            if ((delta & 1ul) != 0) {
                acc_multiplier *= cur_multiplier;
                acc_increment = acc_increment * cur_multiplier + cur_increment;
            }
            cur_increment = (cur_multiplier + 1ul) * cur_increment;
            cur_multiplier *= cur_multiplier;
            delta >>= 1;
        }
        return state * acc_multiplier + acc_increment;
    }

    constexpr static
    std::uint32_t rotate_right_(std::uint32_t value, std::uint32_t rot) noexcept
    {
        constexpr char bits = 32;
        constexpr char mask = bits - 1;
        return (value >> rot) | (value << ((-rot) & mask));
    }

    constexpr static std::uint32_t output_(std::uint64_t state) noexcept
    {
        constexpr char statebits = 64;
        constexpr char outbits = 32;
        constexpr char sparebits = statebits - outbits;
        constexpr char wantedopbits = outbits >= 128 ? 7
                                    : outbits >=  64 ? 6
                                    : outbits >=  32 ? 5
                                    : outbits >=  16 ? 4
                                    :                  3;
        constexpr char opbits = sparebits >= wantedopbits ? wantedopbits
                                                          : sparebits;
        constexpr char amplifier = wantedopbits - opbits;
        constexpr char mask = (1 << opbits) - 1;
        constexpr char topspare = opbits;
        constexpr char bottomspare = sparebits - topspare;
        constexpr char xshift = (topspare + outbits) / 2;

        char rot = static_cast<char>(
          (opbits != 0) ? static_cast<char>(state >> (statebits - opbits)) & mask : 0);
        char amprot = static_cast<char>((rot << amplifier) & mask);
        state ^= state >> xshift;

        uint32_t result = static_cast<uint32_t>(state >> bottomspare);
        result = rotate_right_(result, amprot);
        return result;
    }

    constexpr static std::uint64_t multiplier_ = 6364136223846793005ul;
    constexpr static std::uint64_t increment_ = 1442695040888963407ul;
};

}

#define MONTY_PCG32_HPP
#endif
