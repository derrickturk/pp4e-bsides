#ifndef MONTY_MODEL_HPP

#include <cmath>
#include <cstddef>
#include <array>
#include <functional>
#include <span>
#include <string_view>
#include <type_traits>

namespace monty {

template<std::size_t N>
struct outcome;

template<std::size_t N>
struct project {
    std::string_view name;
    double oil_shrink;
    double gas_shrink;
    double wi;
    double nri;
    double tax_rate;
    std::span<const outcome<N>> outcomes;
};

template<std::size_t N>
using project_ref = std::reference_wrapper<const project<N>>;

template<std::size_t N>
struct outcome {
    double probability;
    std::array<double, N> oil;
    std::array<double, N> gas;
    std::array<double, N> capex;
    std::array<double, N> opex;
    std::span<const project_ref<N>> leads_to;
};

template<std::size_t N>
struct price_deck {
    std::array<double, N> oil_price;
    std::array<double, N> gas_price;
};

template<std::size_t N>
inline constexpr std::array<double, N> net_cashflow(const project<N>& p,
        const outcome<N>& o, const price_deck<N>& pd) noexcept
{
    std::array<double, N> result;
    for (std::size_t i = 0; i < N; ++i) {
        result[i] = (1.0 - p.oil_shrink) * o.oil[i] * pd.oil_price[i] * p.nri
          + (1.0 - p.gas_shrink) * o.gas[i] * pd.gas_price[i] * p.nri;
        result[i] = result[i]
          - p.tax_rate * result[i]
          - o.opex[i] * p.wi * 1000.0
          - o.capex[i] * p.wi * 1000.0;
    }
    return result;
}

template<std::size_t N>
inline constexpr
double npv(const std::array<double, N>& cf, double discount) noexcept
{
    double sum = 0.0;
    for (std::size_t i = 0; i < N; ++i) {
        const double time = i * 1.0 / 12;
        /* this isn't totally kosher: pow is constexpr on GNU,
         * but not required to be by the standard
         */
        sum += cf[i] / std::pow(1.0 + discount, time);
    }
    return sum;
}

}

#define MONTY_MODEL_HPP
#endif
