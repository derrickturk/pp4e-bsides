#ifndef MONTY_PROJECTS_HPP

#include "model.hpp"

namespace monty::projects {
#include "projects/bat_classic"
#include "projects/needs_more_bats"
#include "projects/bat_country"
#include "projects/high_water_mark"
#include "projects/beyond_the_pale"

inline constexpr auto root_projects = std::array {
    project_ref { bat_country },
    project_ref { beyond_the_pale },
};

inline constexpr
std::span<const project_ref<36>> root_projects_span { root_projects };

inline constexpr auto all_projects = std::array {
    project_ref { bat_country },
    project_ref { bat_classic },
    project_ref { needs_more_bats },
    project_ref { beyond_the_pale },
    project_ref { high_water_mark },
};

inline constexpr
std::span<const project_ref<36>> all_projects_span { all_projects };

}

#define MONTY_PROJECTS_HPP
#endif
