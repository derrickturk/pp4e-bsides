#include <iostream>
#include <array>

#include "pcg32.hpp"
#include "model.hpp"
#include "projects.hpp"
#include "sim.hpp"

using namespace monty::projects;

template<std::size_t N>
constexpr std::array<double, N> draw(monty::pcg32 rng) noexcept
{
    std::array<double, N> result;
    for (std::size_t i = 0; i < N; ++i)
        result[i] = rng.uniform_double(0, 1);
    return result;
}

template<std::size_t N, std::size_t TRIALS = 10000>
constexpr
auto npv_trials(monty::pcg32 rng, const monty::price_deck<N> &deck,
        double discount) noexcept
{
    std::array<double, TRIALS> npvs;
    for (std::size_t i = 0; i < TRIALS; ++i) {
        const auto cfs = realize_total_cashflow(rng, root_projects_span, deck);
        npvs[i] = monty::npv(cfs, discount);
    }
    return npvs;
}

constexpr monty::price_deck<36> flat {
    .oil_price = {
        40.0, 40.0, 40.0, 40.0, 40.0, 40.0, 40.0, 40.0, 40.0, 40.0, 40.0, 40.0,
        40.0, 40.0, 40.0, 40.0, 40.0, 40.0, 40.0, 40.0, 40.0, 40.0, 40.0, 40.0,
        40.0, 40.0, 40.0, 40.0, 40.0, 40.0, 40.0, 40.0, 40.0, 40.0, 40.0, 40.0,
    },

    .gas_price = {
        2.50, 2.50, 2.50, 2.50, 2.50, 2.50, 2.50, 2.50, 2.50, 2.50, 2.50, 2.50,
        2.50, 2.50, 2.50, 2.50, 2.50, 2.50, 2.50, 2.50, 2.50, 2.50, 2.50, 2.50,
        2.50, 2.50, 2.50, 2.50, 2.50, 2.50, 2.50, 2.50, 2.50, 2.50, 2.50, 2.50,
    },
};

int main()
{
    constexpr auto npvs = npv_trials(monty::pcg32{}, flat, 0.10);
    for (auto c : npvs)
        std::cout << c << '\n';
}
