#ifndef MONTY_SIM_HPP

#include <stdexcept>

#include "pcg32.hpp"
#include "model.hpp"

namespace monty {

template<std::size_t N>
inline constexpr
const outcome<N>& realize(pcg32& rng, const project<N>& p)
{
    // UGGGGH
    if (p.outcomes.size() == 0)
        throw std::invalid_argument("p: attempt to realize from empty project");

    double q = rng.uniform_double(0.0, 1.0);

    for (const auto& o : p.outcomes) {
        if (o.probability <= q)
            return o;
        q -= o.probability;
    }

    return p.outcomes[p.outcomes.size() - 1];
}

namespace detail {

template<std::size_t N>
inline constexpr
void realize_total_cashflow_into(pcg32& rng,
        std::span<const project_ref<N>> root_projects,
        const price_deck<N>& deck,
        std::array<double, N>& target)
{
    for (const project<N>& p : root_projects) {
        const auto &o = realize(rng, p);
        const auto cf = net_cashflow(p, o, deck);
        for (std::size_t i = 0; i < N; ++i)
            target[i] += cf[i];

        realize_total_cashflow_into(rng, o.leads_to, deck, target);
    }
}

}

template<std::size_t N>
inline constexpr
const std::array<double, N> realize_total_cashflow(pcg32& rng,
        std::span<const project_ref<N>> root_projects,
        const price_deck<N>& deck)
{
    std::array<double, N> result;
    result.fill(0.0);
    detail::realize_total_cashflow_into(rng, root_projects, deck, result);
    return result;
}

}

#define MONTY_SIM_HPP
#endif
